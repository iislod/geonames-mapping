import sys, os
import csv, codecs, re

import unicodedata

import operator
from operator import itemgetter

import utility
import twd97

import jieba
jieba.load_userdict("place.dict")

def FindLongLat(Object):

    LongStr = ['經度', '東經', '採集經度', 'TWD97座標 Y']
    LatStr = ['緯度', '北緯', '採集緯度', 'TWD97座標 X']
    count = 0

    for i in Object:
        Lat = ''
        Long = ''

        for j in Object[i]['Coverage']:
            if utility.is_number(j[1]) and j[0] in LatStr:
                Lat = float(j[1])
            
            if utility.is_number(j[1]) and j[0] in LongStr:
                Long = float(j[1])

            if 'TWD97座標' in j[0] and Long != '' and Lat != '':
                Lat, Long = twd97.towgs84(Lat, Long)

            if j[0] == 'coordinates定位':
                e, n = j[1].split(';')[0].split(' ')
                Lat = float(n[1:])
                Long = float(e[1:])

            if j[0] == u'coordinates\u5b9a\u4f4d':
                token = j[1].split(' ')
                for t in token:
                    if 'E' in t:
                        Long = float(t[1:])
                    if 'N' in t:
                        Lat = float(t[1:-1])

            if j[0] == '座標（GPS)':
                e, n = j[1].split('，')
                Lat = GPStransform_1(n)
                Long = GPStransform_1(e)

            if '北緯' in j[1] and '東經' in j[1] and '遺址之經緯度' not in j[1]:
                n, e = j[1].split(' ')
                temp_n = n.split('北緯')[1].replace('\'', '')
                temp_e = e.split('東經')[1].replace('\'', '')
                if 'unknow' in temp_n or 'unknow' in temp_e:
                    continue
                Lat = GPStransform_3(temp_n)
                Long = GPStransform_3(temp_e)
            
            if '北緯：' in j[1]:
                temp_n = j[1].split('：')[-1]
                Lat = float(temp_n)
            if '東經：' in j[1]:
                temp_e = j[1].split('：')[-1]
                Long = float(temp_e)

            if '遺址之經緯度:' in j[1]:
                if ';' in j[1]:
                    e, n = j[1].split(':')[1].split(';')
                    Lat = GPStransform_2(n)
                    Long = GPStransform_2(e)
                elif '\'\'\'' in j[1]:
                    e, n = j[1].split(':')[1].split("'''")
                    Lat = GPStransform_2(n)
                    Long = GPStransform_2(e)
            
            if Long != '' and Lat != '':
                Object[i]['LongLat'] = (Long, Lat)

    return Object

def GPStransform_1(pos):
    
    gps = 0.0 
    d, ms = pos.split(u'\ufffd')
    gps += float(d)
    m, s = ms.split('\'')
    gps += (float(m) + float(s) /60.0) / 60.0
    
    return round(gps, 6)

def GPStransform_2(pos):
    
    gps = 0.0
    while pos[0] == ' ':
        pos = pos[1:]
    d, ms = pos[2:].split('度')
    if utility.is_number(d):
        gps += float(d)
    token = ms.split('\'')
    m = token[0]
    s = token[1]
    gps += (float(m) + float(s) /60.0) / 60.0
    
    return round(gps, 6)

def GPStransform_3(gpsstr):
    
    gps = 0.0
    if len(gpsstr.split('/')) == 1:
        d = float(gpsstr)
        m = 0.0
        s = 0.0
    elif len(gpsstr.split('/')) == 2:
        d, m = gpsstr.split('/')
        s = 0.0
        if '~' in m:
            m1, m2 = m.split('~')
            m = (float(m1) + float(m2)) / 2.0
        elif '-' in m:
            m1, m2 = m.split('-')
            m = (float(m1) + float(m2)) / 2.0
        elif m == '':
            m = 0.0
    elif len(gpsstr.split('/')) == 3:
        d, m, s = gpsstr.split('/')
        if s == '':
            s = 0.0
        if '~' in m:
            m1, m2 = m.split('~')
            m = (float(m1) + float(m2)) / 2.0
        elif '-' in m:
            m1, m2 = m.split('-')
            m = (float(m1) + float(m2)) / 2.0
        elif m == '':
            m = 0.0
    elif len(gpsstr.split('/')) == 4:
        d, m, s, t = gpsstr.split('/')
        if t == '':
            if s == '':
                s = 0.0
            if '~' in m:
                m1, m2 = m.split('~')
                m = (float(m1) + float(m2)) / 2.0
            elif '-' in m:
                m1, m2 = m.split('-')
                m = (float(m1) + float(m2)) / 2.0
            elif m == '':
                m = 0.0

    gps += float(d) + (float(m) + float(s) /60.0) / 60.0
    
    return round(gps, 6)

def Match(Object, GeoPlaceIndex, ignor):
    
    with open("Transform.txt") as f:
        temp = f.read().splitlines()
    Transform = { i.split(' ')[0]: " ".join(i.split(' ')[1:]) for i in temp}
    
    for i in Object:
        Object[i]['GeoMatchTerm'] = []
        for token in Object[i]['FindTerm']:
            temp = {}
            for w in token:
                if w in GeoPlaceIndex:
                    if w not in temp:
                        temp[w] = 8
                elif w in Transform:
                    if Transform[w] in GeoPlaceIndex:
                        if w not in temp:
                            temp[w] = 10
            Object[i]['GeoMatchTerm'].append(temp.keys())
    
    return Object

def SegmentTerm(Object, GeoPlaceIndex, ignor):
    
    temp = []
    with open("Transform.txt") as f:
        temp = f.read().splitlines()
    Transform = { i.split(' ')[0]: " ".join(i.split(' ')[1:]) for i in temp}

    for i in Object:
        for e, token in enumerate(Object[i]['FindTerm']):
            appendtoken = []
            for term in token:
                if term not in GeoPlaceIndex: #utility.Split_to_String(term) == 4:
                    jieba_result = jieba.cut(term)
                    for w in jieba_result:
                        if w != '' and w != ' ':
                            appendtoken.append(w)
            Object[i]['FindTerm'][e] = list(set(token) | set(appendtoken))

    return Object

def FindTerm(Object, ignor):
    
    for i in Object:
        Object[i]['FindTerm'] = []
        for j in Object[i]['Coverage']:
            temp = []
            if utility.IS_Place(j, ignor) and '??' not in j[1]:
                token = utility.Split_to_String(j[1])
                for t in token:
                    temp.append(utility.clearspace(t))
            Object[i]['FindTerm'].append(list(set(temp)))

    return Object

def Search(Object, GeoPlaceIndex, ignor):

    #### Recgonize terms in Coverage
    #### Result store in Object['FindTerm']
    Object = FindTerm(Object, ignor)
    
    #### Recgonize latitude and longitude term in Coverage  ####
    #### Result store in Object['LongLat']
    Object = FindLongLat(Object)
    
    #### Segment terms in Coverage through jieba  ####
    #### Result store in Object['FindTerm']
    Object = SegmentTerm(Object, GeoPlaceIndex, ignor)

    return Object

if __name__ == "__main__":

    pass
