import sys, os
import csv, codecs, re
import unicodedata
from collections import Counter
import operator
from langdetect import detect
from textblob import TextBlob

### Read Data in CSV FILE ###
### The function will return 3 variables.
### First, csv object, which is stored in a dictionary structure.
### Object -> 'OID': { 'OID': oid,
###                    'DAURI': [(lin_number, content)],
###                    'Title': [(lin_number, content), ..., (lin_number, content)],
###                    'Title::field': [(lin_number, content), ..., (lin_number, content)],
###                    ...: ...,
###                    ...: ...,
###                 }
### Second, column, csv header, stored in a list
### Third, data, raw data of csv file, stored in a 2D list.
def Read(FileName, csvname):
    
    Obj = {}
    data = []
    with open(FileName, newline='') as f:
        dat = csv.reader(f, delimiter=',', quotechar='"')
        for i in dat:
            data.append(i)

    colnames = data[0]
    colnames[0] = "OID"

    objid = 'ID'
    for linenum, line in enumerate(data[1:], 1):
        for e, x in enumerate(line):
            if x != '':
                x = x.replace(",", "，")
                if e == 0:
                    if x not in Obj:
                        objid = x
                        Obj[objid] = {}
                        Obj[objid][colnames[e]] = (str(linenum), x)
                        Obj[objid]['CSVDAT'] = csvname
                else:
                    if colnames[e] not in Obj[objid]:
                        Obj[objid][colnames[e]] = []
                    Obj[objid][colnames[e]].append((str(linenum), x))

    return Obj, colnames, data

### Write data into csv file ###
def CSVWRITETOFILE(X, filename):
    with open(filename, "w", newline='') as f:
        f.write('\ufeff')
        writer = csv.writer(f, delimiter=',', quotechar='"', quoting=csv.QUOTE_ALL)
        writer.writerows(X)

### Test a string is a number string or not ###
def is_number(s):
    try:
        float(s)
        return True
    except ValueError:
        pass
    
    try:
        unicodedata.numeric(s)
        return True
    except (TypeError, ValueError):
        pass
    
    return False

### Test a string is a Chinese string or not ###
def Is_Chinese(term):
    
    isc = 1
    for w in term:
        if w < u'\u4e00' or w > u'\u9fa5':
            isc = 0
    return isc

def Is_Japan(term):
    
    isc = 1
    for w in term:
        if w < u'\u30ff' or w > u'\u30a0':
            isc = 0
    return isc

def detect_term_language(term):

    #prob = {'en': 0, 'zh': 0, 'ja': 0, 'Pun': 0, 'Num': 0, 'NA': 0}
    if len(term) < 3:
        b = TextBlob(term.ljust(3))
        lan = b.detect_language()
    else:
        lan = TextBlob(term).detect_language()

    if lan == 'zh-CN':
        return 'zh'
    return lan
    

### Test a string contains punctuation characters or not ###
def Is_P_in_string(string):
    
    P = 0
    for w in string:
        if unicodedata.category(w).startswith('P'):
            P += 1
    return P

### Return string type  ###
### -1: Nothing string
### 0: Mixture string, contain number, Chinese, English punctuation character.
### 1: English string, with punctuation or number.
### 2: Chinese string, with punctuation or number.
### 3: English string 
### 4: Chinese string
def String_type(term):
    
    E = 0
    C = 0
    P = 0
    N = 0
    for w in term:
        if unicodedata.category(w) == 'Lu' or unicodedata.category(w) == 'Ll': 
            E += 1  ### English character
        elif unicodedata.category(w) == 'Lo':
            C += 1  ### Chinese character
        elif unicodedata.category(w).startswith('P'):
            P += 1  ### Puncaution
        elif unicodedata.category(w) == 'Nd':
            N += 1  ### Number
        
    string_type = -1
    if E != 0 and C == 0 and N == 0:  ### English string
        string_type = 1
    elif E == 0 and C != 0 and N == 0:  ### Chinese string
        string_type = 2
    elif E != 0 and C == 0:
        string_type = 3
    elif E == 0 and C != 0:
        string_type = 4
    elif E != 0 or C != 0 or P != 0 or N != 0:
        string_type = 0  ### Mixture string

    return string_type

### Segment a string  ###
### For example, Happy Birthday生日快樂
### Return ['Happy Birthday', '生日快樂']
def segment_en_zh(string):
    
    newstring = ''
    strtype = unicodedata.category(string[0])
    if strtype == 'Lu':
        strtype = 'Ll'
    for w in string:
        ntype = unicodedata.category(w)
        if ntype == 'Lu':
            ntype = 'Ll'
        if ntype == 'Zs' or ntype.startswith('P'):
            newstring += w
            continue
        if unicodedata.category(w) != strtype:
            newstring += ' '
        newstring += w
        strtype = unicodedata.category(w)
        if strtype == 'Lu':
            strtype = 'Ll'

    return newstring

### Clear space in a string ###
### For example, '  國立臺灣大學  '
### Return '國立臺灣大學'
def clearspace(string):
    
    newstring = ''
    for w in string:
        if not unicodedata.category(w).startswith('C'):
            newstring += w

    string = newstring
    while string.startswith(' '):
        string = string[1:]
    while string.endswith(' '):
        string = string[:-1]

    return string

### Segment a string into sentences.  ###
### '地點: 高雄（高雄縣）、台南市'
### Return ['高雄', '高雄縣', '台南市']
def Split_to_String(string):
    
    if '：' in string:
        string = ":".join(string.split('：')[1:])

    token = []
    string = string.replace('(', '、').replace(')', '、')
    string = string.replace('（', '、').replace('）', '、')
    string = string.replace('-', '、').replace('\'', '、').replace('．', '、').replace('－', '、')
    string = string.replace('，', '、').replace(',', '、').replace('。', "、")
    if String_type(string) == 2:
        string = string.replace('.', '、')
    
    temptoken = string.split('、')
    for t in temptoken:
        if String_type(t) == 2 or String_type(t) == 1:
            token.append(t)

    return list(set(token))

### Test a coverage term is a place or not.
### coverage term: [field, content]
### If field in ignor list, return False
### Else, return True
def IS_Place(term, ignor):
    
    latlong = ['東經', '北緯', '西經', '南緯']
    if term[0] in ignor:
        return 0
    if '：' in term[1]:
        token = term[1].split('：')
        if token[0] in ignor:
            return 0
    for f in latlong:
        if f in term[1]:
            return 0
    return 1

