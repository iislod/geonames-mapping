# Tool for Geonames Mapping

author: Hsin-Ping Chen

# Introduction

This tool is designed to extract place names from the `Coverage` (dc:coverage) field in csv files (converted from xml files of Union Catalog of Digital Archives Taiwan) and map the extracted names to place IDs on [geonames.org] (http://www.geonames.org).

**Note that this tool is only for csv files spllited by projects.**

* Input:
  1. csv files converted from xml files of Union Catalog of Digital Archives Taiwan
  2. placenames from [geonames.org] (http://www.geonames.org)

* Output:
  1. the column `Coverage` and `Coverage::field` from input csv files
  2. the match results in csv files with the following fields and values:

| Field | Value |
| ----- | ----- |
| OID | (from input csv) |
| Coverage::field | (from input csv) |
| Coverage::dwc | (from input csv) |
| Coverage | (from input csv) |
| ExtractName | names extracted from the field `Coverage` with possible language (name@lang), separated by semicolons |
| gns:id | matched geonames id, separated by semicolons |
| gns:nameEN | matched English geonames, separated by semicolons |
| gns:nameZH | matched Chinese geonames, separated by semicolons |
| geo:lat | matched geonames latitude, separated by semicolons |
| geo:long | matched geonames longitude, separated by semicolons |
| Coverage::lat | the latitude extracted from the field `Coverage` (only in the first row of each OID) |
| Coverage::long | the longitude extracted from the field `Coverage` (only in the first row of each OID) |

# Dependencies

* Python 3.4

* Python dependencies

```
pip3 install -r requirements.txt
```

* Only tested on Ubuntu (>= 14.04)

```
sudo apt-get install build-essential python3-dev
```

# Usage

1. Put the geonames.org placename data `CN.txt`, `GeoCountry.txt`, `JP.txt`, and `TW.txt` into a new directory named `geonames` (Placename data can be downloaded from <http://download.geonames.org/export/dump/>).

2. Modify the file `csvlist` with **full paths** of input csv files.

3. Create output directories named `Coverage` and `TermMatch`.

4. Run `python3 coverage.py`

5. Results can be found in the directories mentioned in step 3.
