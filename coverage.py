import sys, os, csv, codecs, re, unicodedata, getpass
from operator import itemgetter
import operator
import utility, geosystem
from langdetect import detect


def GetCoverage(Object, ignor):
    
    NewObject = {}
    for i in Object:

        field = {}
        dwc = {}
        if 'Coverage::field' in Object[i]:
            for j in Object[i]['Coverage::field']:
                field[j[0]] = j[1]
                #fields[j[1]] = 1
        if 'Coverage::dwc' in Object[i]:
            for j in Object[i]['Coverage::dwc']:
                dwc[j[0]] = j[1]
                #fields[j[1]] = 1
        if 'Coverage' in Object[i]:
            Object[i]['CoverageUP'] = []
            for j in Object[i]['Coverage']:
                if '原碑地點' in j[1]:
                    j1, j2 = j[1].split(';')
                    Object[i]['CoverageUP'].append(['', j1, ''])
                    Object[i]['CoverageUP'].append(['', j2, ''])
                    continue
                
                if j[0] in field:
                    Object[i]['CoverageUP'].append([field[j[0]], j[1], 'field'])
                elif j[0] in dwc:
                    Object[i]['CoverageUP'].append([field[j[0]], j[1], 'dwc'])
                else:
                    Object[i]['CoverageUP'].append(['', j[1], ''])

            NewObject[i] = {}
            NewObject[i]['OID'] = i
            NewObject[i]['DAURI'] = Object[i]['DAURI'][0][1]
            NewObject[i]['Coverage'] = Object[i]['CoverageUP']
    
    return NewObject

def ReadGeonameTable():
    
    dat = []
    
    with open("geonames/TW.txt") as f:
        dat = f.read().splitlines()
    with open("geonames/JP.txt") as f:
        dat += f.read().splitlines()
    with open("geonames/CN.txt") as f:
        dat += f.read().splitlines()
    with open("geonames/GeoCountry.txt") as f:
        dat += f.read().splitlines()
    
    geodat = {}
    PlaceIndex = {}
    ll_index = {}

    for i in dat:
        token = i.split('\t')
        geoid = token[0]
        geodat[geoid] = {}
        geodat[geoid]['geonameid'] = geoid
        geodat[geoid]['name'] = token[1]
        geodat[geoid]['asciiname'] = token[2]
        place = token[3].split(',')
        
        for p in place:
            if p not in PlaceIndex and p != '':
                PlaceIndex[p] = geoid

        geodat[geoid]['alternatenames'] = place
        geodat[geoid]['latitude'] = float(token[4])
        geodat[geoid]['longitude'] = float(token[5])
        geodat[geoid]['feature_class'] = token[6]
        geodat[geoid]['feature_code'] = token[7]
        geodat[geoid]['country code'] = token[8]
        geodat[geoid]['cc2'] = token[9]
        geodat[geoid]['admin1_code'] = token[10]
        geodat[geoid]['admin2_code'] = token[11]
        geodat[geoid]['admin3_code'] = token[12]
        geodat[geoid]['admin4_code'] = token[13]
        geodat[geoid]['population'] = token[14]
        geodat[geoid]['elevation'] = token[15]
        geodat[geoid]['dem'] = token[16]
        geodat[geoid]['timezone'] = token[17]
        geodat[geoid]['modification_date'] = token[18]

    ignorword = ['I', 'En', 'B', 'Aodi', '小', '底',  '山', '岡', '後', '枝', '段', '里', '谷', '郡', '間', '池', '洞', '港', '海', '雷']
    for w in ignorword:
        if w in PlaceIndex:
            PlaceIndex.pop(w, None)

    return geodat, PlaceIndex

def DumpCoverage(Object, filename):

    StoreDat = [['OID', 'Coverage::field', 'Coverage']]
    for i in Object:
        for j in Object[i]['Coverage']:
            StoreDat.append([i, j[0], j[1]])

    utility.CSVWRITETOFILE(StoreDat, filename)

def DumpResult(Object, geodat, PlaceIndex, filename):

    with open("Transform.txt") as f:
        temp = f.read().splitlines()
    Transform = { i.split(' ')[0]: " ".join(i.split(' ')[1:]) for i in temp}

    StoreDat = [["OID", "Coverage::field", "Coverage::dwc", "Coverage", "ExtractName", "gns:id", "gns:nameEN", "gns:nameZH", "geo:lat", "geo:long", "Coverage::lat", "Coverage::long"]]

    for oid in Object:
        for e, (j, token) in enumerate(zip(Object[oid]['Coverage'], Object[oid]['FindTerm'])):
            field = ''
            dwc = ''
            if j[2] == 'field':
                field = j[0]
            if j[2] == 'dwc':
                dwc = j[0]

            terms = []
            terms_geodat = {'gnsid': [], 'name': [], 'zh_name': [], 'latitude': [], 'longitude': []}
            for t in token:
                terms.append(t + '@' + utility.detect_term_language(t))
                if t in PlaceIndex:
                    gnsid = PlaceIndex[t]
                elif t in Transform and Transform[t] in PlaceIndex:
                    gnsid = PlaceIndex[Transform[t]]
                else:
                    gnsid = ''
                
                if gnsid != '':
                    zh_name = ''
                    for w in geodat[gnsid]['alternatenames']:
                        if utility.Is_Chinese(w):
                            zh_name = w
                    terms_geodat['gnsid'].append(gnsid)
                    terms_geodat['name'].append(geodat[gnsid]['name'])
                    terms_geodat['zh_name'].append(zh_name)
                    terms_geodat['latitude'].append(str(geodat[gnsid]['latitude']))
                    terms_geodat['longitude'].append(str(geodat[gnsid]['longitude']))
                else:
                    terms_geodat['gnsid'].append('')
                    terms_geodat['name'].append('')
                    terms_geodat['zh_name'].append('')
                    terms_geodat['latitude'].append('')
                    terms_geodat['longitude'].append('')
        
            if e == 0:
                if 'LongLat' in Object[oid]:
                    Long, Lat = Object[oid]['LongLat']
                else:
                    Long, Lat = '', ''
                StoreDat.append([oid, field, dwc, j[1], ";".join(terms), 
                    ";".join(terms_geodat['gnsid']), ";".join(terms_geodat['name']), ";".join(terms_geodat['zh_name']), ";".join(terms_geodat['latitude']), ";".join(terms_geodat['longitude']),
                    Lat, Long])
            else:
                StoreDat.append([oid, field, dwc, j[1], ";".join(terms), 
                    ";".join(terms_geodat['gnsid']), ";".join(terms_geodat['name']), ";".join(terms_geodat['zh_name']), ";".join(terms_geodat['latitude']), ";".join(terms_geodat['longitude']),
                    '', ''])

    if len(StoreDat) > 1:
        utility.CSVWRITETOFILE(StoreDat, filename)

def detectlanguage(Object):
    
    lanset = {}
    count = 0
    for oid in Object:
        for field, value, types in Object[oid]['Coverage']:
            lan = detect(value)
            count += 1
            if lan not in lanset:
                lanset[lan] = 0
            else:
                lanset[lan] += 1

    for i in sorted(lanset):
        print (i, lanset[i]/float(count))


def main():

    ### Read csv list from csvlist ###
    csv.field_size_limit(sys.maxsize)
    csvlist = [line.strip() for line in open('csvlist')]

    geodat, GeoPlaceIndex = ReadGeonameTable()
    ignor = ['TWD97座標 X', 'TWD97座標 Y', '[海拔高度 (公尺)]', 'coordinates定位', 'time period時代', '北緯', '國別代碼', '地質年代', '座標（GPS)', '拍攝日期', '採集方法',
                '採集經度', '採集緯度', '採集深度', '最低海拔', '最大海拔', '最小海拔', '最高海拔', '朝代', '標本館號', '海拔', '深度', '滅絕地質年代', '經度',
                '緯度', '開始地質年代', '東經', '面積', '地質年代', ' 深度（公尺）', '經緯度', '原始採集資訊', ' 深度（公尺）', '經緯度', ### 生物
                '社名', '經度', '緯度', '面積', '所屬族群', ### 人類學
                '地質年代', '產地', '開始地質年代', '滅絕地質年代', '地質分佈', '用途', '地層', '地質分區', ### 地質
                'Temporal', ### 影音
                'coordinates定位', 'time period時代', '座標（GPS)', ### 建築
                '拍攝日期', '朝代', ### 檔案
                '遺址之文化期', '東經', '北緯', '遺址之文化時期', '地形區' ### 考古
            ]


    for completename in csvlist:
        ### get csv category, and split-csv-file ###
        foldername = completename.split('/')[-2]
        csvname = completename.split('/')[-1]
        
        ### Read data ###
        print ("\nProcessing ...%s %s" %(foldername, csvname))
        Object, colnames, data = utility.Read(completename, foldername)
        
        print ("Original number of data", len(Object))
        Object = GetCoverage(Object, ignor)
        if not os.path.exists('Coverage/' + foldername):
            os.mkdir('Coverage/' + foldername)
        DumpCoverage(Object, 'Coverage/' + foldername + '/' + csvname)
        
        Object = geosystem.Search(Object, GeoPlaceIndex, ignor) 
        if not os.path.exists('TermMatch/' + foldername):
            os.mkdir('TermMatch/' + foldername)
        DumpResult(Object, geodat, GeoPlaceIndex, 'TermMatch/' + foldername + '/' + csvname)
        #detectlanguage(Object)

if __name__ == "__main__":
    main()

